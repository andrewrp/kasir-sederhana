module.exports = {
	angular:{
    src:[
      'angular/scripts/dist.js'
    ],
    dest:'angular/scripts/app.min.js'
  },
  html:{
    src:[
      'html/scripts/app.js'
    ],
    dest:'html/scripts/app.min.js'
  }
}