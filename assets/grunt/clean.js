module.exports = {
  angular: ['angular/*'],
  angulars:[
    'angular/styles/**', 
    'angular/less/', 
    'angular/vendor/angular/', 
    'angular/scripts/directives', 
    'angular/scripts/services', 
    'angular/scripts/filters', 
    'angular/index.min.html'
  ],
  html: ['html/*']
};