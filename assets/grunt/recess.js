module.exports = {
	app: {
        files: {
          'app/styles/app.css': [
            'app/less/app.less'
          ]
        },
        options: {
          compile: true
        }
    },
    angular: {
        files: {
            'angular/styles/app.min.css': [
                'app/styles/bootstrap.css',
                'app/styles/bootstrap-additions.css',
                'app/styles/font-awesome.min.css',
                'app/styles/*.css',
            ]
        },
        options: {
            compress: true
        }
    },
    html: {
        files: {
            'html/styles/app.min.css': [
                'app/styles/animate.css',
                'app/styles/bootstrap.css',
                'app/styles/themify-icons.css',
                'app/styles/font-awesome.min.css',
                'app/styles/font.css',
                'app/styles/app.css',
            ]
        },
        options: {
            compress: true
        }
    }
}