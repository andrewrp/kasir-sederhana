<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 01/11/2018
 * Time: 11:06
 */

session_start();
include_once('config/connection.php');

if (isset($_POST['email']) && isset($_POST['password'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];

    $stmt = $connection->prepare("SELECT * FROM users WHERE email = :email");
    $stmt->bindParam(':email', $email);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    if($stmt->rowCount() > 0) {
        $cek_password = password_verify($password, $result['password']);

            if(!$cek_password) {
                echo "<script>alert('Password salah bro!'); history.back();</script>";

            } else {
                $_SESSION['nama'] = $result['nama'];
                $_SESSION['id'] = $result['id'];

                    if ($result['jabatan'] === 'Admin') {
                        $_SESSION['jabatan'] = $result['jabatan'];
                        header('Location: /admin');
                    } else {
                        $_SESSION['jabatan'] = $result['jabatan'];
                        header('Location: /petugas');
                    }
            }
    }


}

?>
