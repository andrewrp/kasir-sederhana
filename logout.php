<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 01/11/2018
 * Time: 12:52
 */

session_start();

session_destroy();
session_unset();

header('Location: index.php');
