<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 01/11/2018
 * Time: 12:10
 */

?>

            <ul class="nav navbar-nav navbar-right m-r-n">
                <li class="dropdown">
                    <a href="#" class="clear no-padding-h" data-toggle="dropdown">
                        <img src="<?php echo BASE_ASSETS. "images/a0.jpg"?>" alt="..." class="navbar-img pull-right">
                        <span class="hidden-sm m-l"><?php echo $_SESSION['nama']; ?></span>
                        <b class="caret m-h-xs hidden-sm"></b>
                    </a>
                    <ul class="dropdown-menu pull-right no-b-t">
                        <li>
                            <a href="<?php echo BASE_URL. "logout.php" ?>">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
