<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 01/11/2018
 * Time: 12:32
 */
?>

<div class="app-aside-body scrollable hover">
    <nav ui-nav>
        <ul class="nav">
            <li class="nav-header h4 m-v-sm">
                Menu
            </li>
            <li>
                <a>

        <span class="pull-right text-muted">
          <i class="fa fa-caret-down"></i>
        </span>
                    <i class="icon glyphicon glyphicon-edit text-lt"></i>
                    <span class="font-normal">Form</span>
                </a>
                <ul class="nav nav-sub bg">
                    <li>
                        <a href="<?php echo BASE_URL. "admin/petugas"?>">List Petugas</a>
                    </li>
                    <li>
                        <a href="<?php echo BASE_URL. "admin/petugas/tambah.php"?>">Tambah Petugas</a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div>
