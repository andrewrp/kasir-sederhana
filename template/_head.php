<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 01/11/2018
 * Time: 12:39
 */
?>

<meta charset="utf-8" />
<title>HeyTayo</title>
<meta name="description" content="app, web app, responsive, responsive layout, admin, admin panel, admin dashboard, flat, flat ui, ui kit, AngularJS, ui route, charts, widgets, components" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<link rel="stylesheet" href="<?php echo BASE_ASSETS. "styles/animate.css"?>" type="text/css" />
<link rel="stylesheet" href="<?php echo BASE_ASSETS. "styles/bootstrap.css"?>" type="text/css" />
<link rel="stylesheet" href="<?php echo BASE_ASSETS. "styles/themify-icons.css"?>" type="text/css" />
<link rel="stylesheet" href="<?php echo BASE_ASSETS. "styles/font-awesome.min.css"?>" type="text/css" />
<link rel="stylesheet" href="<?php echo BASE_ASSETS. "styles/font.css"?>" type="text/css" />
<link rel="stylesheet" href="<?php echo BASE_ASSETS. "styles/app.css"?>" type="text/css" />
