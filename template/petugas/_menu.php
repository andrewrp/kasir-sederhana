<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 01/11/2018
 * Time: 12:32
 */
?>

<div class="app-aside-body scrollable hover">
    <nav ui-nav>
        <ul class="nav">
            <li class="nav-header h4 m-v-sm">
                Menu
            </li>
            <li>
                <a>
        <span class="pull-right text-muted">
          <i class="fa fa-caret-down"></i>
        </span>
                    <i class="icon glyphicon glyphicon-briefcase text-lt"></i>
                    <span class="font-normal">Barang</span>
                </a>
                <ul class="nav nav-sub bg">
                    <li>
                        <a href="<?php echo BASE_URL. "petugas/barang"?>">List Barang</a>
                    </li>
                    <li>
                        <a href="<?php echo BASE_URL. "petugas/barang/tambah.php"?>">Tambah Barang</a>
                    </li>
                </ul>
            </li>
            <li>
                <a>
        <span class="pull-right text-muted">
          <i class="fa fa-caret-down"></i>
        </span>
                    <i class="icon glyphicon glyphicon-edit text-lt"></i>
                    <span class="font-normal">Transaksi</span>
                </a>
                <ul class="nav nav-sub bg">
                    <li>
                        <a href="<?php echo BASE_URL. "petugas/transaksi"?>">List Transaksi</a>
                    </li>
                </ul>
        </ul>
    </nav>
</div>
