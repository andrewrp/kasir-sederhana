<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 01/11/2018
 * Time: 12:41
 */
?>

<div class="p bg-white text-xs">
    <div class="pull-right hidden-xs hidden-sm text-muted">
        <strong>HeyTayo</strong> - Built with AngularJS &amp; Bootstrap  &copy; Copyright 2014
    </div>
    <ul class="list-inline no-margin text-center-xs">
        <li>
            <a href="page.document.html">Documents</a>
        </li>
        <li class="text-muted">-</li>
        <li>
            <a href="http://themeforest.net/user/heyflat/portfolio?ref=heyflat" target="_blank">Purchase</a>
        </li>
    </ul>
</div>
