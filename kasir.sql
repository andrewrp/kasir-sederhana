-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2018 at 12:23 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasir`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `kode_barang` varchar(50) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `kode_barang`, `nama_barang`, `stok`, `harga`, `created_at`) VALUES
(1, 'SBN96', 'Sabun', 3, 3000, '2018-11-01 09:57:14'),
(2, 'AAA', 'SDSD', 40, 4000, '2018-11-02 07:04:37');

-- --------------------------------------------------------

--
-- Stand-in structure for view `detail_transaksi`
-- (See below for the actual view)
--
CREATE TABLE `detail_transaksi` (
`id` int(11)
,`id_barang` int(11)
,`kode_barang` varchar(50)
,`nama_barang` varchar(100)
,`id_petugas` int(11)
,`nama` varchar(100)
,`jumlah` int(11)
,`harga` int(11)
,`created_at` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `id_petugas`, `id_barang`, `jumlah`, `harga`, `created_at`) VALUES
(1, 2, 1, 6, 18000, '2018-11-02 06:16:57'),
(3, 2, 1, 3, 9000, '2018-11-02 07:41:15');

--
-- Triggers `transaksi`
--
DELIMITER $$
CREATE TRIGGER `beli` AFTER INSERT ON `transaksi` FOR EACH ROW BEGIN
UPDATE barang SET stok = stok - NEW.jumlah WHERE id=NEW.id_barang;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `jabatan` enum('Admin','Petugas','','') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `email`, `password`, `jabatan`, `created_at`) VALUES
(1, 'Andrew', 'andrew@nay.com', '$2y$10$tB6Yso2KKeOK5FvmGFXQOuAQgcen3OGSJRwTEWrvCKZKaFYamqTHi', 'Admin', '2018-11-01 04:42:39'),
(2, 'Alfamekdi', 'petugas@nay.com', '$2y$10$tB6Yso2KKeOK5FvmGFXQOuAQgcen3OGSJRwTEWrvCKZKaFYamqTHi', 'Petugas', '2018-11-01 07:05:55'),
(3, 'Nayeon', 'nay@nay.com', '$2y$10$Z.59WL/aKVLLiu4LY6LW9OvdcYnMO4Dw15.JzxU/K7tN0ujPHMKLe', 'Admin', '2018-11-01 08:20:24');

-- --------------------------------------------------------

--
-- Structure for view `detail_transaksi`
--
DROP TABLE IF EXISTS `detail_transaksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `detail_transaksi`  AS  select `transaksi`.`id` AS `id`,`transaksi`.`id_barang` AS `id_barang`,`barang`.`kode_barang` AS `kode_barang`,`barang`.`nama_barang` AS `nama_barang`,`transaksi`.`id_petugas` AS `id_petugas`,`users`.`nama` AS `nama`,`transaksi`.`jumlah` AS `jumlah`,`transaksi`.`harga` AS `harga`,`transaksi`.`created_at` AS `created_at` from ((`transaksi` join `barang` on((`transaksi`.`id_barang` = `barang`.`id`))) join `users` on((`transaksi`.`id_petugas` = `users`.`id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_petugas` (`id_petugas`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_petugas`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
