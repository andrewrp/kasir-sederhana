<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 01/11/2018
 * Time: 11:44
 */
session_start();
include_once "../../config/helper.php";
include_once "../../config/connection.php";

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <?php include "../../template/_head.php" ?>
</head>
<body>
<div class="app app-header-fixed">
    <!-- header -->
    <header id="header" class="app-header navbar bg-primary" role="menu">
        <!-- navbar header -->
        <div class="navbar-header box-shadow-inset dk">
            <button class="pull-right visible-xs" ui-toggle="show" target=".navbar-collapse">
                <i class="ti-settings"></i>
            </button>
            <button class="pull-right visible-xs" ui-toggle="show" target=".app-aside">
                <i class="ti-menu"></i>
            </button>
            <!-- brand -->
            <a class="navbar-brand text-lt">
                <i class="pull-right ti-arrow-circle-down text-sm m-v-xs m-l-xs"></i>
                <i class="glyphicon glyphicon-th-large text-md"></i>
                <img src="images/logo.png" alt="." class="hide">
                <span class="hidden-folded m-l-xs">HeyTayo<sup class="text-xs font-thin">1.2</sup></span>
            </a>
            <!-- / brand -->
        </div>
        <!-- / navbar header -->

        <!-- navbar collapse -->
        <div class="navbar-collapse hidden-xs">
            <!-- nav -->
            <?php include "../../template/petugas/_topnav.php" ?>
            <!-- / nav -->

            <!-- nabar right -->
            <?php include "../../template/petugas/_navbar.php" ?>
            <!-- / navbar right -->
        </div>
        <!-- / navbar collapse -->
    </header>
    <!-- / header -->

    <!-- aside -->
    <aside id="aside" class="app-aside hidden-xs bg-dark lt">
        <div class="app-aside-inner" bs-affix>
            <?php include "../../template/petugas/_menu.php" ?>
        </div>
    </aside>
    <!-- / aside -->

    <!-- content -->
    <div id="content" class="app-content" role="main">

        <div class="p-h-md p-v bg-white box-shadow pos-rlt">
            <h3 class="no-margin">Tambah Barang</h3>
        </div>
        <div class="p-md">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form action="../proses/tambah-barang.php" method="post">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kode Barang</label>
                                    <input type="text" name="kode_barang" class="form-control" id="exampleInputEmail1" placeholder="Kode Barang">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Barang</label>
                                    <input type="text" name="nama_barang" class="form-control" id="exampleInputEmail1" placeholder="Nama Barang">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Jumlah Stok</label>
                                    <input type="number" name="stok" class="form-control" id="exampleInputPassword1" placeholder="Jumlah Stok">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Harga</label>
                                    <input type="number" name="harga" class="form-control" id="exampleInputPassword1" placeholder="Harga">
                                </div>
                                <button type="submit" class="btn btn-info m-b">Tambah</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / content -->

        <!-- footer -->
        <footer id="footer" class="app-footer" role="footer">
            <?php include "../../template/_footer.php" ?>
        </footer>
        <!-- / footer -->

    </div>

    <?php include "../../template/_script.php" ?>

</body>
</html>
