<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 02/11/2018
 * Time: 12:43
 */

session_start();
include "../../config/connection.php";

    $id_petugas = $_SESSION['id'];
    $id_barang = $_POST['id_barang'];
    $jumlah = $_POST['jumlah_beli'];
    $harga = $_POST['harga'];
    $total_harga = $harga * $jumlah;

    $stmt = $connection->prepare("INSERT INTO transaksi(id_petugas,id_barang,jumlah,harga) VALUES(?,?,?,?)");
    $data = array(
        $id_petugas,
        $id_barang,
        $jumlah,
        $total_harga
    );
    $result = $stmt->execute($data);

    if ($result) {
        echo "<script>alert('Sukses Bro'); history.back();</script>";
    } else {
        echo "<script>alert('Gagal Bro'); history.back();</script>";
    }