<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 01/11/2018
 * Time: 16:30
 */
include_once('../../config/connection.php');

$kode_barang = $_POST['kode_barang'];
$nama_barang = $_POST['nama_barang'];
$stok = $_POST['stok'];
$harga = $_POST['harga'];

$stmt = $connection->prepare("INSERT INTO barang(kode_barang,nama_barang,stok,harga) VALUES(?,?,?,?)");
$data = array(
    $kode_barang,
    $nama_barang,
    $stok,
    $harga
);
$result = $stmt->execute($data);

if ($result) {
    echo "<script>alert('Sukses Bro'); history.back();</script>";
} else{
    echo "<script>alert('Gagal Bro'); history.back();</script>";
}
