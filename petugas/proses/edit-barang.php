<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 01/11/2018
 * Time: 17:07
 */

include_once "../../config/connection.php";

$id = $_POST['id'];
$kode_barang = $_POST['kode_barang'];
$nama_barang = $_POST['nama_barang'];
$stok = $_POST['stok'];
$harga = $_POST['harga'];

$stmt = $connection->prepare("UPDATE barang SET kode_barang=?,nama_barang=?, stok=?, harga=? WHERE id=?");
$data = array(
    $kode_barang,
    $nama_barang,
    $stok,
    $harga,
    $id
);
$result = $stmt->execute($data);

if ($result) {
    echo "<script>alert('Berhasil Di Edit'); history.back();</script>";
} else {
    echo "<script>alert('Gagal Bro'); history.back();</script>";
}
