<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 01/11/2018
 * Time: 11:44
 */
session_start();
include_once "../../config/helper.php";
include_once "../../config/connection.php";

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <?php include "../../template/_head.php" ?>
</head>
<body>
<div class="app app-header-fixed">
    <!-- header -->
    <header id="header" class="app-header navbar bg-primary" role="menu">
        <!-- navbar header -->
        <div class="navbar-header box-shadow-inset dk">
            <button class="pull-right visible-xs" ui-toggle="show" target=".navbar-collapse">
                <i class="ti-settings"></i>
            </button>
            <button class="pull-right visible-xs" ui-toggle="show" target=".app-aside">
                <i class="ti-menu"></i>
            </button>
            <!-- brand -->
            <a class="navbar-brand text-lt">
                <i class="pull-right ti-arrow-circle-down text-sm m-v-xs m-l-xs"></i>
                <i class="glyphicon glyphicon-th-large text-md"></i>
                <img src="images/logo.png" alt="." class="hide">
                <span class="hidden-folded m-l-xs">HeyTayo<sup class="text-xs font-thin">1.2</sup></span>
            </a>
            <!-- / brand -->
        </div>
        <!-- / navbar header -->

        <!-- navbar collapse -->
        <div class="navbar-collapse hidden-xs">
            <!-- nav -->
            <?php include "../../template/admin/_topnav.php" ?>
            <!-- / nav -->

            <!-- nabar right -->
            <?php include "../../template/admin/_navbar.php" ?>
            <!-- / navbar right -->
        </div>
        <!-- / navbar collapse -->
    </header>
    <!-- / header -->

    <!-- aside -->
    <aside id="aside" class="app-aside hidden-xs bg-dark lt">
        <div class="app-aside-inner" bs-affix>
            <?php include "../../template/admin/_menu.php" ?>
        </div>
    </aside>
    <!-- / aside -->

    <!-- content -->
    <div id="content" class="app-content" role="main">

        <div class="p-h-md p-v bg-white box-shadow pos-rlt">
            <h3 class="no-margin">Tambah Petugas</h3>
        </div>
        <div class="p-md">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form action="../proses/tambah-petugas.php" method="post">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Fullname</label>
                                    <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter Fullname">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Jabatan</label>
                                    <select class="form-control" name="jabatan">
                                        <option value="Admin">Admin</option>
                                        <option value="Petugas">Petugas</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-info m-b">Tambah</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / content -->

        <!-- footer -->
        <footer id="footer" class="app-footer" role="footer">
            <?php include "../../template/_footer.php" ?>
        </footer>
        <!-- / footer -->

    </div>

    <?php include "../../template/_script.php" ?>

</body>
</html>
