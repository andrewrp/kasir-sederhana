<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 01/11/2018
 * Time: 14:12
 */

include_once('../../config/connection.php');

$nama = $_POST['name'];
$email = $_POST['email'];
$password = $_POST['password'];
$password_hash = password_hash($password, PASSWORD_BCRYPT);
$jabatan = $_POST['jabatan'];

$stmt = $connection->prepare("INSERT INTO users(nama,email,password,jabatan) VALUES(?,?,?,?)");
$data = array(
    $nama,
    $email,
    $password_hash,
    $jabatan
);
$result = $stmt->execute($data);
var_dump($result);

if ($result) {
    echo "<script>alert('Sukses Bro'); history.back();</script>";
} else{
    echo "<script>alert('Gagal Bro'); history.back();</script>";
}

?>