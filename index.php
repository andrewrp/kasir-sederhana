<?php
include_once "config/helper.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>HeyMetro | Responsive Web Admin App with AngularJS And Bootstrap</title>
    <meta name="description" content="app, web app, responsive, responsive layout, admin, admin panel, admin dashboard, flat, flat ui, ui kit, AngularJS, ui route, charts, widgets, components" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <link rel="stylesheet" href="<?php echo BASE_ASSETS. "assets/html/styles/animate.css"?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo BASE_ASSETS. "styles/bootstrap.css"?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo BASE_ASSETS. "styles/themify-icons.css"?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo BASE_ASSETS. "styles/font-awesome.min.css"?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo BASE_ASSETS. "styles/font.css"?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo BASE_ASSETS. "styles/app.css"?>" type="text/css" />
</head>
<body>
<div class="app app-header-fixed">

    <div class="container">
        <div class="center-block w-xl w-auto-xs m-b-lg">
            <div class="text-2x m-v-lg text-primary"><i class="glyphicon glyphicon-th-large text-xl"></i> HeyTayo</div>
            <div class="m-b text-sm">
                Sign in
            </div>
            <form name="form" action="proses-login.php" method="post">
                <div class="form-group m-b-xs">
                    <label class="hide">Email</label>
                    <input type="email" name="email" placeholder="Someone@example.com" class="form-control" ng-model="user.email" required>
                </div>
                <div class="form-group m-b-xs">
                    <label class="hide">Password</label>
                    <input type="password" name="password" placeholder="Password" class="form-control" ng-model="user.password" required>
                </div>
                <button type="submit" class="btn btn-info p-h-md m-v-lg">Sign in</button>
            </form>
        </div>
    </div>
    <div class="app-footer" ng-include="'views/footer.html'"></div>


</div>

<script src="<?php echo BASE_ASSETS. "scripts/jquery.min.js"?>"></script>
<script src="<?php echo BASE_ASSETS. "scripts/bootstrap.js"?>"></script>
<script src="<?php echo BASE_ASSETS. "scripts/ui-load.js"?>"></script>
<script src="<?php echo BASE_ASSETS. "scripts/ui-jp.config.js"?>"></script>
<script src="<?php echo BASE_ASSETS. "scripts/ui-jp.js"?>"></script>
<script src="<?php echo BASE_ASSETS. "scripts/ui-nav.js"?>"></script>
<script src="<?php echo BASE_ASSETS. "scripts/ui-toggle.js"?>"></script>

</body>
</html>